**Description:**
Simple test deployment of rke2 server and rke2 agent using vagrant and Rancher's rke2 quickstart.  Note this is of course for demo purposes. 

Enhancements could include:  Variable server and agent counts.  Merging the vagrants into one Vagrantfile.  Setting token dynamically.  Setting up ssh for sharing secrets.  Staging dependencies for a faster/air gapped deployment. 



**Prerequisites:**

 
 - [ ] Download/Install Vagrant - https://www.vagrantup.com/downloads.html

    

 - [ ] Download/Install VirtualBox - https://www.virtualbox.org/wiki/Downloads

        

 - [ ] Clone Project directory - git@gitlab.com:masonboro-public/dsocc-share/vagrants/simple-rke2-cluster.git

  

**Start Vagrants**
Run the run.sh script, or start vagrants manually with vagrant up from the server and agent directories.  If executing manually allow the server to start and respond ready prior to starting the agent.

It should take around 15-20 minutes for the server to bootstrap. Informative warnings/errors may appear throughout.

  

**Tracking Progress:**
From the project's server directory, run vagrant ssh.

 - Get Current Status: `systemctl status rke2-server`
 - Monitor Progress: `journalctl -u rke2-server -f`
 - Watch Node Changes: `KUBECONFIG=/etc/rancher/rke2/rke2.yaml
   /var/lib/rancher/rke2/bin/kubectl get nodes -w`

  
  

You may set KUBECONFIG path on run time of kubectl, set KUBECONFIG as an environment variable or copy/setup the binary and config as below:

```
mkdir -p ~/.kube/
cp /etc/rancher/rke2/rke2.yaml ~/.kube/config
cp /var/lib/rancher/rke2/bin/kubectl /usr/local/bin/kubectl
kubectl get nodes

```


