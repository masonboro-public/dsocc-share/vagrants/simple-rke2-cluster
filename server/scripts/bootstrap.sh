wait_for_ready(){
        until KUBECONFIG=/etc/rancher/rke2/rke2.yaml /var/lib/rancher/rke2/bin/kubectl get node | grep -w Ready
        do
          #systemctl status rke2-server -l | tail -n 3
          sleep 30
        done
}

mkdir -p /etc/rancher/rke2/

cat > /etc/rancher/rke2/config.yaml <<EOF
write-kubeconfig-mode: "0644"
advertise-address: 192.168.99.4
token: 9j219jsad1nm0hjsdhnfsdfjh218
EOF

#yum update /y

curl -sfL https://get.rke2.io | sh -

systemctl enable rke2-server.service
systemctl start rke2-server.service

wait_for_ready

PATH=/var/lib/rancher/rke2/bin:$PATH
KUBECONFIG=/etc/rancher/rke2/rke2.yaml

cat /var/lib/rancher/rke2/server/node-token